import json
from pathlib import Path

import animal


class Writer:
    def __init__(self, obj: animal.Serializable):
        self.obj = obj.serialize()

    @staticmethod
    def write(self, obj: animal.Serializable, *args) -> bool:
        pass


class SqlWriter(Writer):
    connection = None

    def __init__(self, connection, obj: animal.Serializable):
        super().__init__(obj)
        self.connection = connection

    def execute_query(self):
        cursor = self.connection.cursor()
        query = "INSERT INTO animal(name, type, noise) VALUES(%(name)s, %(type)s, %(noise)s)"
        try:
            cursor.execute(query, self.obj)
        except Exception as e:
            print("query: {query}, data: {data}, cant be writen in db")
            print(e)
            return False
        return True

    @staticmethod
    def write(obj: animal.Serializable,connection):
        return SqlWriter(connection, obj).execute_query()


class JSONWriter(Writer):
    def __init__(self, path, obj: animal.Serializable):
        super().__init__(obj)
        self.path = path

    @staticmethod
    def write(obj: animal.Serializable, path):
        # si fichier pas trouver on le cree
        file = Path(path)
        writer = JSONWriter(path, obj)
        if file.is_file():
            with open(file, "r") as f:
                animals = json.load(f)
            animals.append(writer.obj)
        else:
            animals = [writer.obj]
        with open(file, "w") as f:
            json.dump(animals, f, indent=4)
        return True

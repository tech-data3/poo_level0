class Serializable:
    def serialize(self):
        pass
    @staticmethod
    def desserialize(dict):
        pass

class Animal(Serializable):
    noise = ""

    def __init__(self,name=None, **kwargs):
        if name:
            self.name = name
        else:
            raise ValueError

    def speak(self) -> str:
        return f"{self.noise}"

    def serialize(self) -> dict:
        return {"name": self.name, "type": type(self).__name__}


    @staticmethod
    def desserialize(dict):
        if dict.get("type"):
            constructor = globals()[dict.pop("type")]
            return constructor(**dict)
        return None


class Pinguin(Animal):
    noise = "quick quick"


class Monkey(Animal):
    noise = "houhou"


class Cow(Animal):
    noise = "meuh"


class Herd(Serializable):
    instances = []
    def __init__(self, animals=None):
        Herd.instances.append(self)
        self.animals = []

    def count(self):
        return len(self.animals)

    def add(self, animal: Animal) -> Animal | None:
        if not any(ins.contains(animal) for ins in Herd.instances):
            self.animals.append(animal)
            return animal
        return None

    def remove(self, animal: Animal) -> Animal | None:
        # enfaite on pop ? je pense ca parce que tu retourne un animal ou rien (rien je supose que c'est si rien a ete supprimer)
        try:
            return self.animals.pop(self.animals.index(animal))
        except ValueError:
            print(f"nop, {animal} n'est pas ici, on peut pas l'enlever")
            return None

    def contains(self, animal: Animal) -> Animal | None:
        return animal if animal in self.animals else None

    def serialize(self):
        return [animal.serialize() for animal in self.animals]
    @staticmethod
    def desserialize(list):
        return [Herd.desserialize(i) for i in list]
